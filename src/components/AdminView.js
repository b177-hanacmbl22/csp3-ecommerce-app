import ArchiveProduct from './ArchiveProduct';
import { useEffect, useState } from 'react';
import { Button, Table, Form, Modal, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import './AdminView.css';

export default function AdminView(props) {
	const {productsData, fetchData} = props;

	const [products, setProducts] = useState([]);

	const [productId, setProductId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [imageLink, setImageLink] = useState('');

	useEffect(() => {
		const productsArr = productsData.map(product => {
			return (
				<tr key={product._id}>
					<td><img id="admin-view" src={product.imageLink} className="img-fluid"/></td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td> {
							(product.isActive === true)
							? <span>Available</span>
							: <span>Sold Out</span>
						}
					</td>
					<td>
						<Button id="update-btn" size="sm" onClick={ ()=> openEdit(product._id) }>Update</Button>
						<ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData} />
					</td>
				</tr>
			);
		});
		
		setProducts(productsArr);
	}, [productsData]);

	
	const [showAdd, setShowAdd] = useState(false);

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false); 
 
	const addProduct = (e) => {
		e.preventDefault();

		fetch('https://pacific-spire-89064.herokuapp.com/products/add', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				imageLink: imageLink
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully added'
				});

				closeAdd()
				fetchData()
			}else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Please try again'
				});

				fetchData()
			}

			setName('');
			setDescription('');
			setPrice(0);
			setImageLink('');
		});
	};

	const [showEdit, setShowEdit] = useState(false);

	const openEdit = (productId)  => {
		fetch(`https://pacific-spire-89064.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImageLink(data.imageLink);
		});

		setShowEdit(true);
	};

	const closeEdit = () => setShowEdit(false);

	const editProduct = (e, productId) => {
		e.preventDefault();

		fetch(`https://pacific-spire-89064.herokuapp.com/products/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				imageLink: imageLink
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully updated'
				});

				fetchData();
				closeEdit();
			} else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Please try again'
				});

				fetchData();
				closeEdit();
			}
		});
	};

	return (
		<Container>
			<div>
				<h2 id="admin-title" className="text-center mt-3 mb-3">Products | Admin Dashboard</h2>
			</div>

			<div className="d-flex justify-content-center mb-2">
			<Button id="add-product-btn"  onClick={openAdd}>Add New Product</Button>
			</div>
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control 
							      type="text"
							      required
							      value={name}
							      onChange={e => setName(e.target.value)}
							 />
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
							      type="text"
							      required
							      value={description}
							      onChange={e => setDescription(e.target.value)}
							 />
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control 
							      type="number"
							      required
							      value={price}
							      onChange={e => setPrice(e.target.value)}
							 />
						</Form.Group>

						<Form.Group>
							<Form.Label>Image Link</Form.Label>
							<Form.Control 
							      type="text"
							      required
							      value={imageLink}
							      onChange={e => setImageLink(e.target.value)}
							 />
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button id="edit-product-close-button" variant="secondary" onClick={closeAdd}>Close</Button>
						<Button id="edit-product-submit-button" variant="success" type="submit">Submit</Button>
					</Modal.Footer>

				</Form>
			</Modal>

			<Table>
				<thead>
					<tr className="text-center">
						<th>Image</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Action</th>
					</tr>
				</thead>

				<tbody>
					{products}
				</tbody>
			</Table>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control 
								type="text"
								required
								value={name}
								onChange={e => setName(e.target.value)}
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text"
								required
								value={description}
								onChange={e => setDescription(e.target.value)}
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="number"
								required
								value={price}
								onChange={e => setPrice(e.target.value)}
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Image Link</Form.Label>
							<Form.Control 
								type="text"
								required
								value={imageLink}
								onChange={e => setImageLink(e.target.value)}
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Save Changes</Button>
					</Modal.Footer>
				</Form>
			</Modal>
			
		</Container>
	);
};