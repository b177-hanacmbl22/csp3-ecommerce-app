import { useEffect, useState } from 'react';
import { Table, Container, Button } from 'react-bootstrap';
import './AdminView.css';

export default function AllOrders({orderProp}) {
	const [orderList, setOrderList] = useState([]);

	useEffect(() => {
		const ordersArr = orderProp.map(order => {
			return (
				<tr className="text-center" key={order._id}>
					<td>{order.userId}</td>
					<td>{order.purchasedOn}</td>
					<td>{order.products[0].productName}</td>
					<td>{order.products[0].productPrice}</td>
					<td>{order.products[0].quantity}</td>
					<td>{order.totalAmount}</td>
					<td>{(order.isPaid) ? 'Paid' : 'Not Paid'}</td>
				</tr>
			);
		});
		
		setOrderList(ordersArr);
	}, [orderProp]);

	return (
		<Container>
			<h2 id="admin-title" className="text-center my-3">All Orders</h2>

			<Table size="sm" className="mb-5">
				<thead className="text-center">
					<tr id="all-orders-table-title">
						<th>User ID</th>
						<th>Date Ordered</th>
						<th>Item</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Total</th>
						<th>Status</th>
					</tr>
				</thead>

				<tbody>
					{orderList}
				</tbody>
			</Table>
			<a href="/products" id="return-admin-btn" className="btn">Return to Admin Dashboard</a>

		</Container>
	);
};