import { Button, Card, Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Products.css';


export default function Product ({productProp}) {

	const {imageLink ,name, description, price, _id} = productProp

	return (
		<Container className="my-5">
		<Card id="all-products">
		<Card.Body>
			<Row>
				<Col>
					<Card.Img id="product-img" src={imageLink} alt=""/>
				</Col>
				<Col id="product-text-card">				
					<Card.Title id="product-text">{name}</Card.Title>
					<p id="product-description" className="text-justify">{description}</p>
					<p id="product-text">&#8369; {price}</p>
					<Button id="product-button1" variant="flat" as={Link} to={`/products/${_id}`}>Add to Cart</Button>
				</Col>
			</Row>
			</Card.Body>
			</Card>
		</Container>
	);
};