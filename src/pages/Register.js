import { useContext, useEffect, useState } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import UserContext from './../UserContext';
import Swal from 'sweetalert2';
import './Register.css';

export default function Register() {
	const { user } = useContext(UserContext);

	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');

	const [isDisabled, setIsDisabled] = useState(true);

	const navigate = useNavigate();

	useEffect(() => {
		if ((username !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [username, password, verifyPassword]);

	function registerUser(e) {
		e.preventDefault();

		fetch('https://pacific-spire-89064.herokuapp.com/users/register', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				username: username,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				Swal.fire({
					title: 'Registration Successful',
					icon: 'success',
					text: 'You have successfully registered'
				});

				navigate('/login');
			} else {
				Swal.fire({
					title: 'Registration Failed',
					icon: 'error',
					text: 'Please try again'
				});
			}
		});
	};

	return (
		(user.accessToken !== null) ?
			<Navigate to="/products" />
			:
			<Container id="register" className="mb-5">
			<h1 className="text-center">Register</h1>
			<Form id="register-form" onSubmit={e => registerUser(e)}>
				<Form.Group className="mb-3 w-75" >
					<Form.Label>Username:</Form.Label>
					<Form.Control 
						type="username"
						placeholder="Enter your username"
						required
						value={username}
						onChange={e => setUsername(e.target.value)}
					/>
				</Form.Group>

				<Form.Group className="mb-3 w-75" >
					<Form.Label>Password:</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Enter your password"
						required
						value={password}
						onChange={e => setPassword(e.target.value)}
					/>
				</Form.Group>

				<Form.Group className="mb-3 w-75" >
					<Form.Label>Verify Password:</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Verify your password"
						required
						value={verifyPassword}
						onChange={e => setVerifyPassword(e.target.value)}
					/>
				</Form.Group>

				<Button id="register-button" variant="light" type="submit" disabled={isDisabled}>Submit</Button>
				<h8 id="register-link">  Already have an account? <Link className='link' to={'/login'}>Login</Link> here.</h8>
			</Form>
			</Container>
	);
};