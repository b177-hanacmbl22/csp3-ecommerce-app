import AdminView from './../components/AdminView';
import UserView from './../components/UserView';
import { useContext, useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from './../UserContext';
import './ProductPage.css';

export default function ProductPage() {
	const { user } = useContext(UserContext);

	const [ allProducts, setAllProducts ] = useState([]);

	const fetchData = () => {
		fetch('https://pacific-spire-89064.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {
			setAllProducts(data);
		});
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
	<>
		
		
		{(user.isAdmin === true) ?
			<AdminView productsData={allProducts} fetchData={fetchData} />
			:
			<>
				<h1 className="my-3 text-center">Products</h1>
				<UserView productsData={allProducts} />
			</>
		}
	</>
	);
};