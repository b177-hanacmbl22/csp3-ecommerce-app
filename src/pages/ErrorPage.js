export default function ErrorPage() {
	return (
		<>
			<h1 className="mt-5 text-white">Page Not Found</h1>
			
			<div><img className="pageNotFound" alt="404" /></div>
		</>
	);
};