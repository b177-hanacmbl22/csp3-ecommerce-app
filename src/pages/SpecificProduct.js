import { useContext, useEffect, useState } from 'react';
import { Button, Form, Container, Row, Col, Card } from 'react-bootstrap';
import { Link, Navigate, useNavigate, useParams } from 'react-router-dom';
import UserContext from './../UserContext';
import Swal from 'sweetalert2';
import './SpecificProduct.css';

export default function SpecificProduct() {

	const {user} = useContext(UserContext);

	const {productId} = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);

	const navigate = useNavigate();

	useEffect(() => {
		fetch(`https://pacific-spire-89064.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});
	}, []);
	
	const addToCart = (e, productId) => {
		e.preventDefault();

		fetch('https://pacific-spire-89064.herokuapp.com/orders/createOrder', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: name,
				productPrice: price,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {
				Swal.fire({
					title: 'Added',
					icon: 'success',
					text: 'Successfully added to cart'
				});

				navigate('/products');
			} else {
				Swal.fire({
					title: 'Registration Failed',
					icon: 'error',
					text: 'Please try again'
				});
			}
		});
	};

	return (
		(user.accessToken !== null) ?
			(user.isAdmin === true) ?
				<Navigate to="/products" />
				:
				<Row className="justify-content-center">
				<Col className="text-center" lg={8}>
				<Card id="card" className=" mt-5">
				<Form className="text-center" onSubmit={e => addToCart(e, productId)}>
					<h1 id="product-title" className="mt-5">{name}</h1>
					<p>{description}</p>
					<p>Price: ₱ {price}</p>
					
					<Form.Group id="cart-form" className="mb-3 w-75">
						<Form.Label>Quantity:</Form.Label>
						<Form.Control 
							type="number"
							required
							value={quantity}
							onChange={e => setQuantity(parseInt(e.target.value))}
						/>
					</Form.Group>

					<Form.Group id="cart-form" className="mb-3 w-75">
						<Form.Label>Total:</Form.Label>
						<Form.Control 
							type="number"
							required
							value={price * quantity}
							disabled
						/>
					</Form.Group>
					

					{(quantity > 0) ?						
							<Button id="cart-btn" variant="flat" className="mb-5" type="submit">Add to Cart</Button>
						:
							<Button id="cart-btn" variant="flat" className="mb-5" disabled>Add to Cart</Button>
						
					}
					
				</Form>
				</Card>		
				</Col>
				</Row>
			:
			
			<Row className="justify-content-center">
			<Col className="text-center" lg={8}>
			<Card id="card" className=" mt-5">
			<Form className="text-center">
				<h1 id="product-title" className="mt-5">{name}</h1>
				<p>{description}</p>
				<p>Price: ₱ {price}</p>
				
				<Form.Group id="cart-form" className="mb-3">
					<Form.Label>Quantity:</Form.Label>
					<Form.Control 
						type="number"
						required
						value={quantity}
						onChange={e => setQuantity(parseInt(e.target.value))}
					/>
				</Form.Group>

				<Form.Group id="cart-form" className="mb-3">
					<Form.Label>Total:</Form.Label>
					<Form.Control 
						type="number"
						required
						value={price * quantity}
						disabled
					/>
				</Form.Group>

				<Button id="cart-btn" variant="flat" className="mb-5" as={Link} to="/login">Login to Order</Button>
			</Form>	
			</Card>		
			</Col>
			</Row>
			
	);
};