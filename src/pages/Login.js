import { useContext, useEffect, useState } from 'react';
import { Button, Form, Container } from 'react-bootstrap';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import UserContext from './../UserContext';
import Swal from 'sweetalert2';
import './Login.css';

export default function Login() {
	const {user, setUser} = useContext(UserContext);

	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');

	const [isDisabled, setIsDisabled] = useState(true);	

	const navigate = useNavigate();

	useEffect( () => {
		if(username !== '' && password !== ''){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [username, password]);

	function authentication(e) {
		e.preventDefault();

		fetch('https://pacific-spire-89064.herokuapp.com/users/login', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				username: username,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data.accessToken !== undefined) {
				localStorage.setItem('accessToken', data.accessToken);
				setUser({
					accessToken: data.accessToken
				});

				Swal.fire({
					title: 'Login Success',
					icon: 'success',
					text: 'You are now login!'
				});

				fetch('https://pacific-spire-89064.herokuapp.com/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					if (data.isAdmin === true) {
						localStorage.setItem('isAdmin', data.isAdmin);

						setUser({
							isAdmin: data.isAdmin
						});

						navigate('/');
					} else {
						navigate('/');
					}
				});
			} else {
				Swal.fire({
					title: 'Login Failed',
					icon: 'error',
					text: 'Check your credentials'
				});
			}
		});
	};

	return (
		(user.accessToken !== null) ?
			<Navigate to="/products" />
			:
			<Container id="login" className="mb-5">
			<h1 className="mt-2 text-center">Login</h1>
			<Form id="login-form" onSubmit={e => authentication(e)}>
				
				
				<Form.Group className="mb-3 w-75">
					<Form.Label>Username:</Form.Label>
					<Form.Control 
						type="username"
						placeholder="Enter your username"
						required
						value={username}
						onChange={e => setUsername(e.target.value)}
					/>
				</Form.Group>
				<Form.Group className="mb-3 w-75">
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Enter your password"
						required
						value={password}
						onChange={e => setPassword(e.target.value)}
					/>
				</Form.Group>

				<Button id="login-button" variant="light" type="submit" disabled={isDisabled}>Submit</Button>
				<h8 id="login-link"> Don't have an account? <Link className='link' to={'/register'}>Register</Link> here.</h8>
			
			</Form>
			</Container>
	);
};